import React from 'react';
import './App.css';
import Timer from './komponen kelas/Timer';

function App() {
    return (
        <div className="App container-fluid">
            <Timer />
        </div>
    );
}

export default App;

import React from 'react';
import '../bootstrap/bootstrap.css';

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nilaiInput: '',
            nilaiTampil: 0,
        };
    }

    handleChange(event) {
        const inputValue = event.target.value;
        this.setState({
            nilaiInput: parseInt(inputValue),
            nilaiTampil: parseInt(inputValue) * 60,
        });
    }

    timer() {
        this.interval = setInterval(() => {
            this.setState({ nilaiTampil: this.state.nilaiTampil - 1 });
        }, 1000);

        if (this.state.nilaiInput === '' || this.state.nilaiInput === 0) {
            alert('masukkin dulu boss nomornya!!');
            clearInterval(this.interval);
        }
    }

    reset() {
        this.setState({
            nilaiInput: '',
            nilaiTampil: 0,
        });
        clearInterval(this.interval);
    }

    stop() {
        clearInterval(this.interval);
    }

    render() {
        if (isNaN(this.state.nilaiTampil)) {
            this.setState({ nilaiTampil: 0 });
        }

        return (
            <div className="row justify-content-center align-items-center p-5">
                <div className="col-sm-8 bg-dark p-5 rounded">
                    <h3 className="text-danger">Masukkan menit timer anda</h3>
                    <input
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        type="number"
                        value={this.state.nilaiInput}
                    />

                    <div className="timer mb-3 text-primary">
                        <p>Jadi {this.state.nilaiTampil} detik yaa</p>
                    </div>

                    <button
                        onClick={() => {
                            this.timer();
                        }}
                        className="mx-3 btn btn-success btn-sm"
                    >
                        start
                    </button>

                    <button
                        onClick={() => {
                            this.reset();
                        }}
                        className="btn btn-danger btn-sm"
                    >
                        reset
                    </button>

                    <button
                        onClick={() => {
                            this.stop();
                        }}
                        className="mx-3 btn btn-warning btn-sm"
                    >
                        stop
                    </button>
                </div>
            </div>
        );
    }
}

export default Timer;
